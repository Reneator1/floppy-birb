﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

	public float speed;

	public float speedMultiplicator = 1000;

	public Boolean successorSpawned = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector2.left.x* speed*(speedMultiplicator/1000)*Time.deltaTime,0,0);
	}
	
	void FixedUpdate()
	{
//		GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(-1 * speed * Time.deltaTime,0,0));
	}
}
