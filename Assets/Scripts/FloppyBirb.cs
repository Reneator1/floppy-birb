﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FloppyBirb : MonoBehaviour
{

    public bool levitating = false;
    public float jumpVelocity = 10;

    public static GameOverEvent OnGameOver = new GameOverEvent();
    public ScoreEvent OnScore = new ScoreEvent();


    //    public GameRestartEvent OnGameRestart;

    [Serializable]
    public class GameOverEvent : UnityEvent
    {

    }

    [Serializable]
    public class ScoreEvent : UnityEvent<Single, GameObject>
    {
    }


    //    [Serializable]
    //    public class GameStartEvent : UnityEvent
    //    {
    //    }


    private void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {
        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().RefreshPlayer(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        if (Input.GetKeyDown(KeyCode.L))
        {
            ToggleLevitation();
        }

    }

    private void ToggleLevitation()
    {
        {
            levitating = !levitating;
            SetLevitating(levitating);

        }
    }

    public void SetLevitating(bool levitation)
    {
        if (levitation)
        {
            levitating = true;
            GetComponent<Rigidbody2D>().constraints = UnityEngine.RigidbodyConstraints2D.FreezePosition;
        }
        else
        {
            levitating = false;
            GetComponent<Rigidbody2D>().constraints = UnityEngine.RigidbodyConstraints2D.FreezePositionX;
        }
    }


    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Obstacle>() != null)
        {
            OnGameOver.Invoke();
        }
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, jumpVelocity);
            GetComponent<AudioSource>().Play(0);
            GetComponent<Animator>().Play("Up", -1, 0f);
        }

        GetComponent<Collider>();

    }

    public void SetGravity(Single single)
    {
        GetComponent<Rigidbody2D>().gravityScale = single;
    }

    public void SetJumpForce(Single single)
    {
        jumpVelocity = single;
    }

    public void ResolveScoreZone(Single single, GameObject scoreZone)
    {
        OnScore.Invoke(single, scoreZone);
    }


}