﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour
{
    public GameObject GameOverPanel;
    public Text timerText;

    public Text scoreText;

    private Boolean paused;
    private String currentSceneName;

    private static Single score;



    private void Awake()
    {
        score = 0;
    }


    public void Restart()
    {
        UnpauseGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        // Scene currentScene = SceneManager.GetActiveScene();
        // if (currentSceneName == null)
        // {
        //     currentSceneName = currentScene.name;
        // }

        // SceneManager.sceneUnloaded += OnSceneUnloaded;
        // SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(currentSceneName));

    }

    void OnSceneUnloaded(Scene scene)
    {
        if (currentSceneName == scene.name)
            SceneManager.LoadScene(currentSceneName, LoadSceneMode.Additive);
    }


    public void TogglePause()
    {
        if (Time.timeScale == 1)
        {
            PauseGame();
        }
        else
        {
            UnpauseGame();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        paused = true;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
        paused = false;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (paused && Input.GetKeyDown(KeyCode.Space))
        {
            Restart();
        }

        timerText.text = formatTime(Time.timeSinceLevelLoad);
    }

    public void GameOver()
    {
        GameOverPanel.SetActive(true);
        PauseGame();
    }

    string formatTime(float time)
    {
        string timeText = time.ToString();
        string[] texts = timeText.Split('.');
        return texts[0];
    }

    public void IncreaseScore(Single single){
        score+=single;
        Debug.Log("Player score increased by "+single+ " new Score: "+score);
        scoreText.text = score.ToString(); 
    }

}