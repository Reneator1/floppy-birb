﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObstacleKillZone : MonoBehaviour
{

	public DestroyEvent destroyEvent;
	public FloorEvent floorEvent;

	
	
	
	[Serializable]
	public class DestroyEvent : UnityEvent<GameObject>
	{
	}
	
	[Serializable]
	public class FloorEvent : UnityEvent<GameObject>
	{
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals("Obstacle"))
		{
			destroyEvent.Invoke(other.gameObject);
		}

	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals("Floor"))
		{
			if (other.gameObject.GetComponent<Obstacle>().successorSpawned)
			{
				return;
			}
			floorEvent.Invoke(other.gameObject);
		}

	}
	
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals("Floor"))
		{
			destroyEvent.Invoke(other.gameObject);
		}

	}
}
