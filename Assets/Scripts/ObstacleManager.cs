﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class ObstacleManager : MonoBehaviour
{
    public Single obstacleSpeed;
    public Single ObstacleHeight;
    public Single obstacleWidth;
    public GameObject obstacleSpawnerTop;
    public GameObject ObstacleSpawnerBottom;
    public GameObject ScoreZoneSpawner;
    public GameObject ScoreZone;
    public GameObject GameStateManager;
    public Single obstacleSpawningIntervalInSec;
    public Single obstacleSpawningHeight;

    private DateTime lastSpawnTime;
    public Boolean obstacleSpawning;
    private ObstacleList obstacleCache;
    public List<GameObject> obstaclesPublic = new List<GameObject>();

    public String obstacleBotPrefab = "Prefabs/ObstacleSmall";
    public String obstacleTopPrefab = "Prefabs/ObstacleTop";


    private void Awake()
    {
        InitializeObstacleCache();
        obstacleCache.AddAll(obstaclesPublic);
        RefreshObstacles();
    }

    // Update is called once per frame
    private void Update()
    {
        if (obstacleSpawning)
        {
            if (lastSpawnTime.AddSeconds(obstacleSpawningIntervalInSec) < DateTime.Now)
            {
                createNextObstacles();
                lastSpawnTime = DateTime.Now;
            }

            //if TimeSinceLastSpawn 
        }
    }


    public void OnSpeedChange(Single single)
    {
        obstacleSpeed = single;
        RefreshObstacles();
    }

    public void OnFloorLeave(GameObject floor)
    {
        InitializeObstacleCache();
        obstacleCache.Remove(floor);
        SpawnNewFloor(floor);
        Destroy(floor);
    }

    public void SpawnNewFloor(GameObject gameObject)
    {
        InitializeObstacleCache();
        SpawnObjectAdjacentToRight(gameObject);
    }

    public void SpawnObjectAdjacentToRight(GameObject floorObject)
    {
        GameObject newFloor = Instantiate<GameObject>(floorObject);
        newFloor.transform.position =
            new Vector3(floorObject.transform.position.x + floorObject.GetComponent<Renderer>().bounds.size.x,
                floorObject.transform.position.y);
        newFloor.transform.localScale = floorObject.transform.localScale;
        floorObject.GetComponent<Obstacle>().successorSpawned = true;
        obstacleCache.Add(newFloor);
    }

    public void createNextObstacles()
    {
        InitializeObstacleCache();

        SpawnObstacle(GenerateObstacleFromPrefab(obstacleTopPrefab), obstacleSpawnerTop);
        SpawnObstacle(GenerateObstacleFromPrefab(obstacleBotPrefab), ObstacleSpawnerBottom);
        SpawnObstacle(ScoreZone, ScoreZoneSpawner);
        RefreshObstacles();
    }

    private void InitializeObstacleCache()
    {
        if (obstacleCache == null)
        {
            obstacleCache = this.gameObject.AddComponent<ObstacleList>();
        }
    }

    private void RefreshObstacles()
    {
        InitializeObstacleCache();

        obstacleCache.RefreshObstacles(obstacleSpeed);
    }


    private void SpawnObstacle(GameObject objectToSpawn, GameObject spawnerObject)
    {
        GameObject obj = Instantiate<GameObject>(objectToSpawn);
        Vector3 size = obj.GetComponent<Collider2D>().bounds.size;
        obj.transform.position = spawnerObject.transform.position;
        obstacleCache.Add(obj);
    }

    public void DestroyAndRemove(GameObject other)
    {
        obstacleCache.Remove(other);
        Destroy(other);
    }

    public void ResolveScoreZone(Single score, GameObject zone)
    {
        this.GameStateManager.GetComponent<GameStateManager>().IncreaseScore(score);
        DestroyAndRemove(zone);
    }


    private GameObject GenerateObstacleFromPrefab(String prefab)
    {
        return Resources.Load<GameObject>(prefab);
    }


    public void StartObstacleSpawning()
    {
        //Start Loop For Obstacle-Spawning
        obstacleSpawning = true;
    }

    public void StopObstacleSpawning()
    {
        obstacleSpawning = false;
    }

    public void SetObstacleSpawning(Boolean value)
    {
        obstacleSpawning = value;
    }

    public void DespawnObstacles()
    {
        obstacleCache.RemoveAndDestroyAll();
    }

    public void RestartObstacleSpawning()
    {
        //resetAllParameters to Start
    }

    public void SetObstacleSpawningInterval(Single single)
    {
        obstacleSpawningIntervalInSec = single;
    }

    public void SetObstacleSpawnHeight(Single single)
    {
        obstacleSpawningHeight = single;
    }
}

class ObstacleList : MonoBehaviour
{
    private List<GameObject> obstacles = new List<GameObject>();


    public void RemoveAndDestroyAll()
    {
        foreach (GameObject obstacle in obstacles)
        {
            Destroy(obstacle);
        }

        obstacles = new List<GameObject>();
    }

    public void Add(GameObject gameObject)
    {
        obstacles.Add(gameObject);
    }

    public void AddAll(List<GameObject> gameObjects)
    {
        obstacles.AddRange(gameObjects);
    }

    public void Remove(GameObject gameObject)
    {
        List<GameObject> toKeep = new List<GameObject>();

        foreach (GameObject obj in obstacles)
        {
            if (!obj.Equals(gameObject))
            {
                toKeep.Add(obj);
            }
        }

        obstacles = toKeep;
    }

    public void RefreshObstacles(float speed)
    {
        foreach (GameObject obstacle in obstacles)
        {
            obstacle.GetComponent<Obstacle>().speed = speed;
        }
    }
}