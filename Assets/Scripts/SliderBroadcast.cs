﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBroadcast : MonoBehaviour {
	private void Awake()
	{
		Slider slider = GetComponent<Slider>();
		slider.onValueChanged.Invoke(slider.value);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
