﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerManager : MonoBehaviour
{

	public FloppyBirb player;
	private GameStateManager gameStateManager;


	void Awake()
	{
		LoadGameStateManager();
		SubscribeToPlayer();

	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RefreshPlayer(GameObject player)
	{
		this.player = player.GetComponent<FloppyBirb>();
	}

	public void LoadGameStateManager()
	{
		gameStateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}


	public void SetPlayerGravity(Single single)
	{
		player.SetGravity(single);
	}

	public void SetPlayerJumpForce(Single single)
	{
		player.SetJumpForce(single);
	}

	public void SetPlayerIsLevitating(bool levitating)
	{
		player.SetLevitating(levitating);
	}

	public void SubscribeToPlayer()
	{
		FloppyBirb.OnGameOver.AddListener(gameStateManager.GameOver);
		
		//TODO: better learn scenemanagement to understand how to use Controllers for persistent playerdata
		//TODO: Trying to implement PLayerPrefs for saving data inbetween scenes.
		//https://answers.unity.com/questions/1248297/how-to-reload-a-scene-but-keeping-others-already-l.html
	}	
}
