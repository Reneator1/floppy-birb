﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpNRun : MonoBehaviour
{
    public double RunSpeedMax;

    public float RunSpeed = 2;

    public double RunAcceleration;

    public double jumpHeight;
    public double jumpHeightMax;
    public float jumpVelocity = 10;

    public float runStep = 0.2f;

    private bool grounded = true;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
//        Movement();
        Jump();
       
        

      
    }

    void Movement()
    {
        if (Input.GetAxis("Horizontal") == 1)
        {
            Debug.Log("right");
            
            transform.Translate(new Vector2(1,0) * RunSpeed * Time.deltaTime, Space.World); 
        }


        if (Input.GetAxis("Horizontal") == -1)
        {
            Debug.Log("left");
            transform.Translate(new Vector2(-1,0) * RunSpeed * Time.deltaTime, Space.World); 
            
        }
        
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, jumpVelocity);
        }

        GetComponent<Collider>();

    }
}