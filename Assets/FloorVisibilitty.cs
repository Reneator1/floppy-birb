﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloorVisibilitty : MonoBehaviour {
	
	public DestroyEvent destroyEvent;

	
	[Serializable]
	public class DestroyEvent : UnityEvent<GameObject>
	{
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	private void OnTriggerExit2D(Collider2D other)
	{
		destroyEvent.Invoke(other.gameObject);
	}
}
